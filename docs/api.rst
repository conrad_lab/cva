|project_name| API
------------------

Main class
__________
.. automodule:: CVA.cva
    :members:

Reporting
_________
.. automodule:: CVA.reporting
    :members:

Utility functions
_________________
.. automodule:: CVA.utils
    :members:

Configuration file
__________________
.. automodule:: CVA.config
    :members: