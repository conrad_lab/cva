#Copyright (C) 2019  Soeren Lukassen

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

import codecs
import json
import os

import numpy as np
import pandas as pd
import urllib3
from keras.utils import to_categorical
from sklearn.preprocessing import LabelEncoder, Normalizer


def one_hot_encoder(classes: np.ndarray):
    """
    A utility function to transform dense labels into sparse (one-hot encoded) ones. This wraps LabelEncoder from sklearn.preprocessing and to_categorical from keras.utils. If non-integer labels are supplied, the fitted LabelEncoder is returned as well.

    :param classes: A 1D numpy ndarray of length samples containing the individual samples class labels.
    :return: A 2D numpy ndarray of shape samples x classes and a None type if class labels were integers or a fitted instance of class sklearn.preprocessing.LabelEncder
    """
    if classes.dtype == 'O':
        l_enc = LabelEncoder()
        classes_encoded = l_enc.fit_transform(classes)
        classes_onehot = to_categorical(classes_encoded)
        return classes_onehot, l_enc
    elif classes.dtype in ['int16', 'int32', 'int64']:
        classes_onehot = to_categorical(classes)
        return classes_onehot, None


def download_gmt(url: str, destination: str or None=None, file_name: str or None=None, replace: bool = False):
    """
    Utility function to download .gmt pathway files into the correct subfolder.

    :param url: The URL of the gmt files.
    :param destination: Destination directory. If left blank, defaults to 'gmts/'
    :param file_name: The file name to write to disk. If left blank, is left unchanged from the download source files name.
    :param replace: Boolean indicating whether to overwrite if the file name already exists.
    :return: None
    """
    if destination is None:
        destination = 'gmts/'
    assert os.path.isdir(destination), print('Destination does not exist')
    if file_name is None:
        file_name = url.split('/')[-1]
    file = os.path.join(destination, file_name)
    if not replace:
        assert not os.path.exists(file), \
            print('File already exists and replace is set to False')
    http = urllib3.PoolManager()
    r = http.request('GET', url, preload_content=False)
    with open(file, 'wb') as out:
        while True:
            data = r.read()
            if not data:
                break
            out.write(data)
    r.release_conn()
    return None


def get_wikipathways(organism: str, destination: str or None=None, file_name: str or None=None, replace=False):
    """
    Utility function to quickly download WikiPathway data. A wrapper for download_gmt.

    :param organism: String (one of 'hs', 'mm', 'rn', 'dr', 'dm') to select download of the pathway data for either human, mouse, rat, zebra fish, or fruit fly.
    :param destination: Destination directory. If left blank, defaults to 'gmts/'
    :param file_name: The file name to write to disk. If left blank, is left unchanged from the download source files name.
    :param replace: Boolean indicating whether to overwrite if the file name already exists.
    :return: None
    """
    assert organism in ['hs', 'mm', 'rn', 'dr', 'dm'], print('Organism not found')
    if organism == 'hs':
        url = 'http://data.wikipathways.org/current/gmt/wikipathways-20190610-gmt-Homo_sapiens.gmt'
        download_gmt(url=url, destination=destination, file_name=file_name, replace=replace)
    return None


def gmt_to_json(infile: str, outfile: str or None=None):
    """
    Utility function to convert .gmt pathway files to json and write them to disk.

    :param infile: Path of the input file to convert.
    :param outfile: Output path of the corresponding .json
    :return: None
    """
    assert os.path.isfile(infile), print('Input file does not exist')
    if outfile is None:
        outfile = infile.split('.', 1)[0] + '.json'
    gmt = []
    with open(infile, 'r') as f:
        for line in f:
            gmt.append(line.strip().split('\t', 2))
    f.close()
    gmt = np.asarray(gmt)
    genes = []
    for line in gmt[:, 2].tolist():
        genes.append(line.split('\t'))
    genes = np.expand_dims(np.asarray(genes), axis=1)
    gmt = np.hstack([gmt[:, :2], genes]).tolist()
    json.dump(gmt, codecs.open(outfile, 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=True, indent=4)


def read_json(infile: str):
    """
    Utility function to read a .json and report the result as a list.

    :param infile: Path to the input .json.
    :return: list of gne-pathway mappings
    """
    assert os.path.isfile(infile), print('Input file does not exist')
    with open(infile) as json_file:
        data: list = json.load(json_file)
    json_file.close()    
    return data


def normalize_count_matrix(exprs: np.ndarray):
    """
    Utility function to normalize a count matrix for the samples to sum to one. Wrapper for sklearn.preprocessing.Normalizer

    :param exprs: 2D numpy ndarray of shape samples x genes containing the gene expression values to be normalized to unit norm
    :return: a 2D numpy array of shape samples x genes of normalized expression values and a fitted instance of sklearn.preprocessing.Normalizer to be used for reconverting expression values after training CVA
    """
    norm = Normalizer(norm='l1', copy=False)
    norm_exprs = norm.fit_transform(exprs)
    return norm_exprs, norm


def load_exprs(path: str, sep: str = ',', order: str = 'cg'):
    """
    Utility function to load expression matrices, extract gene names, and return both

    :param path: Path to the expression matrix
    :param sep: Separator used in the expression file (default: ',')
    :return: a 2D numpy ndarray with the expression values and a pandas index object containing the ordered gene names
    """
    # TODO: Update Docstring
    assert os.path.isfile(path), print('Invalid file path')
    assert order in ['cg', 'gc']
    ext = path.split('.')[-1]
    assert ext in ['csv', 'tsv', 'pkl', 'feather'], print('Unrecognized file format. Currently supported formats include: csv, tsv, pkl and feather.')
    if ext == 'csv':
        exprs = pd.read_csv(path, sep=sep, header=0, index_col=0)
    elif ext == 'tsv':
        exprs = pd.read_csv(path, sep='\t', header=0, index_col=0)
    elif ext == 'pkl':
        exprs = pd.read_pickle(path)
    elif ext == 'feather':
        exprs = pd.read_feather(path)        
    if not order == 'cg':
        exprs = exprs.T

    return np.asarray(exprs), exprs.columns


def calculate_elbow(weights: np.ndarray, negative: bool = False):
    """
    Calculates the position of the elbow point for each weight matrix.

    :param weights: A 1D or 2D numpy ndarray of length genes or shape neurons x genes containing weight mappings
    :param negative: Boolean indicating whether to return negatively enriched indices (default: False)
    :return: Returns an integer (1D input) or 1D numpy ndarray (2D input) with the position of the elbow point along a sorted axis
    """
    if weights.ndim == 1:
        if negative:
            weights_current = np.sort(np.abs(weights[weights < 0] / np.min(weights[weights < 0])))
            weights_index = np.arange(len(weights_current)) / np.max(len(weights_current))
            distance = weights_index - weights_current
            return len(weights_index) - np.argmax(distance)
        else:
            weights_current = np.sort(np.abs(weights[weights >= 0] / np.max(weights[weights >= 0])))
            weights_index = np.arange(len(weights_current)) / np.max(len(weights_current))
            distance = weights_index - weights_current
            return np.argmax(distance) + np.sum(weights < 0)
    if weights.ndim > 1:
        distances = []
        if negative:
            weights_index = np.arange(weights.shape[1]) / np.min(weights.shape[1])
            for neuron in range(weights.shape[0]):
                weights_current = np.sort(np.abs(weights[neuron, weights < 0] / np.max(weights[neuron, weights < 0])))
                distance = weights_index - weights_current
                distances.append(len(weights_index) - np.argmax(distance))
        else:
            weights_index = np.arange(weights.shape[1]) / np.max(weights.shape[1])
            for neuron in range(weights.shape[0]):
                weights_current = np.sort(np.abs(weights[neuron, weights < 0] / np.max(weights[neuron, weights < 0])))
                distance = weights_index - weights_current
                distances.append(np.argmax(distance) + np.sum(weights < 0))
        return distances


def assert_config(config: dict):
    assert len(config['INPUT_SHAPE']) == 2, print('Input shape has wrong dimensionality')
    assert type(config['ENCODER_SHAPE']) == list, \
        print('Encoder shape is not a list')
    assert len(config['ENCODER_SHAPE']) >= 1, \
        print('Missing encoder dimensions')
    assert type(config['DECODER_SHAPE']) == list,\
        print('Decoder shape is not a list')
    assert len(config['DECODER_SHAPE']) >= 1, \
        print('Missing decoder dimensions')
    assert config['ACTIVATION'] in ['relu', 'elu', 'sigmoid', 'tanh', 'softmax', 'selu'], \
        print('Unknown hidden layer activation function')
    assert config['LAST_ACTIVATION'] in ['relu', 'elu', 'sigmoid', 'tanh', 'softmax', 'selu'], \
        print('Unknown final layer activation function')
    assert type(config['DROPOUT']) in [int, type(None), float], print('Invalid value for dropout')
    if config['DROPOUT']:
        assert config['DROPOUT'] < 1, print('Dropout too high')
    assert type(config['LATENT_SCALE']) == int and config['LATENT_SCALE'] >= 1, \
        print('Invalid value for latent scale. Please choose an integer larger than or equal to one')
    assert type(config['BATCH_SIZE']) == int and config['BATCH_SIZE'] >= 1, \
        print('Invalid value for batch size. Please choose an integer larger than or equal to one')
    assert type(config['EPOCHS']) == int and config['EPOCHS'] >= 1, \
        print('Invalid value for epochs. Please choose an integer larger than or equal to one')
    assert type(config['STEPS_PER_EPOCH']) in [type(None), int], \
        print('Invalid value for steps per epoch. Please choose None or an integer larger than or equal to one')
    assert type(config['VALIDATION_SPLIT']) in [float, type(None)], \
        print('Invalid value for validation split. Please choose None or a float value smaller than one')
    assert type(config['LATENT_OFFSET']) in [float, int], \
        print('Please choose a number for the latent offset')
    assert config['DECODER_BIAS'] in ['last', 'all', 'none'], \
        print('Invalid value for decoder bias. Please choose all, none, or last')
    assert config['DECODER_REGULARIZER'] in ['none', 'l1', 'l2', 'l1_l2', 'var_l1', 'var_l2', 'var_l1_l2'], \
        print('Invalid value for decoder regularizer. Please choose one of '
              'none, l1, l2, l1_l2, var_l1, var_l2, or var_l1_l2')
    if config['DECODER_REGULARIZER'] != 'none':
        assert type(config['DECODER_REGULARIZER_INITIAL']) == float, \
            print('Please choose a float value as (initial) decoder regularizer penalty')
    assert config['BASE_LOSS'] in ['mse', 'mae'], \
        print('Please choose mse or mae as base loss')
    assert type(config['DECODER_BN']) == bool, \
        print('Please choose True or False for the decoder batch normalization')
    assert type(config['CB_LR_USE']) == bool, \
        print('Please choose True or False for the learning rate reduction on plateau')
    assert type(config['CB_ES_USE']) == bool, \
        print('Please choose True or False for the early stopping callback')
    if config['CB_LR_USE']:
        assert type(config['CB_LR_FACTOR']) == float, \
            print('Please choose a decimal value for the learning rate reduction factor')
        assert type(config['CB_LR_PATIENCE']) == int and config['CB_LR_PATIENCE'] >= 1, \
            print('Please choose an integer value equal to or larger than one for the learning rate reduction patience')
        assert type(config['CB_LR_MIN_DELTA']) == float or config['CB_LR_MIN_DELTA'] == 0, \
            print('Please choose a floating point value or 0 for the learning rate reduction minimum delta')
    if config['CB_ES_USE']:
        assert type(config['CB_ES_PATIENCE']) == int and config['CB_ES_PATIENCE'] >= 1, \
            print('Please choose an integer value equal to or larger than one for the early stopping patience')
        assert type(config['CB_ES_MIN_DELTA']) == float or config['CB_ES_MIN_DELTA'] == 0, \
            print('Please choose a floating point value or 0 for the early stopping minimum delta')
    if config['CB_LR_USE'] or config['CB_ES_USE']:
        assert config['CB_MONITOR'] in ['loss', 'val_loss'], \
            print('Please choose loss or val_loss as metric to monitor for callbacks')
    assert type(config['MULTI_GPU']) in ['bool', 'int'] 
